/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package it.sephiroth.android.library.tooltip;

public final class R {
	public static final class attr {
		public static final int ttlm_arrowRatio = 0x7f010006;
		public static final int ttlm_backgroundColor = 0x7f010003;
		public static final int ttlm_cornerRadius = 0x7f010005;
		public static final int ttlm_defaultStyle = 0x7f010000;
		public static final int ttlm_padding = 0x7f010001;
		public static final int ttlm_strokeColor = 0x7f010002;
		public static final int ttlm_strokeWeight = 0x7f010004;
	}
	public static final class color {
		public static final int ttlm_default_background_color = 0x7f060000;
		public static final int ttlm_default_stroke_color = 0x7f060001;
	}
	public static final class dimen {
		public static final int ttlm_default_corner_radius = 0x7f070001;
		public static final int ttlm_default_padding = 0x7f070000;
		public static final int ttlm_default_stroke_weight = 0x7f070002;
	}
	public static final class layout {
		public static final int tooltip_textview = 0x7f03005f;
	}
	public static final class style {
		public static final int ToolTipLayoutDefaultStyle = 0x7f080000;
	}
	public static final class styleable {
		public static final int[] TooltipLayout = { 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006 };
		public static final int TooltipLayout_ttlm_arrowRatio = 5;
		public static final int TooltipLayout_ttlm_backgroundColor = 2;
		public static final int TooltipLayout_ttlm_cornerRadius = 4;
		public static final int TooltipLayout_ttlm_padding = 0;
		public static final int TooltipLayout_ttlm_strokeColor = 1;
		public static final int TooltipLayout_ttlm_strokeWeight = 3;
	}
}
